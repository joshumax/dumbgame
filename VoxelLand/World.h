#pragma once

#define _CRT_RAND_S

#include <vector>
#include <time.h>
#include <Windows.h>
#include "Structures.h"

class World {
private:
	int max_height, max_width;
	typedef struct ChunkLine {
		int Height;
		char Material;
		bool contains_chunk_data;
		std::vector<char> chunk_data;
	};
	std::vector<ChunkLine*> chunks;
	char materialsList[5];
	Structures structures;
	void generateWorldStructures(const int height, const char materialCursor);
public:
	World(int world_height, int world_width);
	~World();
	void generateWorld();
	void printData();
	void printTerrainAtChunk(DWORD winHandle, int multiplier);
};