#include "World.h"
#include <iostream>

void World::generateWorldStructures(const int height, const char materialCursor)
{
	Structures::StructureData* world_struct = structures.getRandomStructure();
	char chunk_dyn[MAX_STRUCTURE_HEIGHT]
		[MAX_STRUCTURE_WIDTH] = { { 0 } };

	// Replace bitmap with material
	for (int i = 0; i < MAX_STRUCTURE_HEIGHT; i++)
		for (int j = 0; j < MAX_STRUCTURE_WIDTH; j++)
			chunk_dyn[i][j] = (world_struct->data[i][j] == 0 ? ' ' : materialsList[materialCursor]);

	// Loop through and add structure to world
	for (int it_w = 0; it_w < MAX_STRUCTURE_WIDTH; it_w++) {
		chunks.push_back(new ChunkLine{
			height,
			materialsList[materialCursor],
			true
		});
		for (int it_h = MAX_STRUCTURE_HEIGHT; it_h >= 0; --it_h) {
			(chunks.back())->chunk_data.push_back(chunk_dyn[it_h][it_w]);
		}
	}
}

World::World(int world_height, int world_width)
{
	// Initialization lists
	// Muck-up VS2013
	max_height = world_height;
	max_width = world_width;
	srand(time(NULL)); // rand() compat
	// Please, use rand_s() instead!
	materialsList[0] = '#';
	materialsList[1] = '*';
	materialsList[2] = '@';
	materialsList[3] = '&';
	materialsList[4] = '~';
}

World::~World()
{
	// Destroy chunks
	for (auto &it : chunks)
		delete it; // DELETE IT! ;P
}

void World::generateWorld()
{
	char materialCursor = 0; // Material cursor
	//int startHeight = rand() % max_height;
	for (int i = 0; i <= max_width; i++) {
		if (i == max_width) {
			chunks.push_back(new ChunkLine{
				max_height,
				'#',
				false
			});
			break;
		}
		switch (i) {
		case 0:
			chunks.push_back(new ChunkLine{
				max_height,
				'#',
				false
			});
			break;
		case 1:
			materialCursor = rand() % sizeof(materialsList);
			// Set the initial chunk
			chunks.push_back(new ChunkLine{
				(rand() % max_height - 2) + 1,
				materialsList[materialCursor],
				false
			});
			break;
		default:
		{
			// Generate terrain
			// ChunkLine* chunkp = new ChunkLine;
			if (!(rand() % 4)) // 25% chance of material change
				materialCursor = rand() % sizeof(materialsList);
			switch (rand() % 3) {
			case 0: // Down 1 block
			{
				if ((chunks.back())->Height <= 1) // Not < 1 high
					chunks.push_back(new ChunkLine{
					2,
					materialsList[materialCursor],
					false
				});
				chunks.push_back(new ChunkLine{
					(chunks.back())->Height - 1,
					materialsList[materialCursor],
					false
				});
				break;
			}
			case 1: // Same block height
				// Generate structure?
				if (!(rand() % 10) &&
					(chunks.back()->Height + MAX_STRUCTURE_HEIGHT) < max_height &&
					i + MAX_STRUCTURE_WIDTH + 2 < max_width) {
					chunks.push_back(new ChunkLine{
						(chunks.back())->Height,
						materialsList[materialCursor],
						false
					});
					// Generate a structure
					generateWorldStructures(chunks.back()->Height, materialCursor);
				}
				chunks.push_back(new ChunkLine{
					(chunks.back())->Height,
					materialsList[materialCursor],
					false
				});
				break;
			case 2: // Up 1 block
			{
				if ((chunks.back())->Height >= max_height - 2) // Not > max_height-2
					chunks.push_back(new ChunkLine{
					max_height - 3,
					materialsList[materialCursor],
					false
				});
				chunks.push_back(new ChunkLine{
					(chunks.back())->Height + 1,
					materialsList[materialCursor],
					false
				});
				break;
			}
			}
			break;
		}
		}
	}
}

void World::printData()
{
	for (auto &it : chunks)
		std::cout << "Height: " << it->Height
		<< " Material: " << it->Material
		<< " Chunk data? " << std::to_string(it->contains_chunk_data)
		<< " X: " << std::string(it->chunk_data.begin(), it->chunk_data.end())
		<< std::endl;
}

void World::printTerrainAtChunk(DWORD winHandle, int multiplier)
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(winHandle), &csbi);
	COORD cursor;
	int renderStart = multiplier * csbi.dwSize.X;

	for (int render_counter = 0; render_counter <= csbi.dwSize.X;
		render_counter++) {
		for (int ground_pos = csbi.dwSize.Y; ground_pos >= (csbi.dwSize.Y - chunks[render_counter]->Height);
			ground_pos--) {
			COORD screen_coords;
			screen_coords.X = render_counter;
			screen_coords.Y = ground_pos;
			SetConsoleCursorPosition(GetStdHandle(winHandle), screen_coords);
			std::cout << std::string(chunks[render_counter]->Material, 1) << std::endl;
		}
	}
}