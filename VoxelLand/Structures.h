/*
 * Welcome weary traveler, I bet you're
 * Wondering what this here file's about...
 * Well, the solution to your inquiry is quite
 * Simple, it holds bitmap info for structures
 * To be generated on the world. -Josh
 */

#pragma once

#include <vector>
#include <string>
#include <array>

#define MAX_STRUCTURE_WIDTH 8
#define MAX_STRUCTURE_HEIGHT 8

#define STRUCTURES_BUILTIN private
class Structures {
private:
	typedef struct StructureData {
		std::string name;
		char data[MAX_STRUCTURE_HEIGHT]
			[MAX_STRUCTURE_WIDTH];
	};

	std::vector <StructureData*> structures;
	friend class World;
protected:
	void addStructure(std::string name, char data[MAX_STRUCTURE_HEIGHT][MAX_STRUCTURE_WIDTH])
	{
		StructureData* st_data = new StructureData {
			name,
			{ { 0 } }
		};
		structures.push_back(st_data);
		for (int i = 0; i < MAX_STRUCTURE_HEIGHT; i++)
			for (int j = 0; j < MAX_STRUCTURE_WIDTH; j++)
				(structures.back())->data[i][j] = data[i][j];
	}
public:
	Structures()
	{
		initDefault();
	}

	~Structures()
	{
		for (auto &del : structures)
			delete del;
	}

	StructureData* getRandomStructure()
	{
		return structures[rand()%structures.size()];
	}

	StructureData* getStructureAtIndex(int i)
	{
		if (i >= 0 && i < structures.size())
			return structures.at(i);
		return nullptr;
	}
STRUCTURES_BUILTIN:
	void initDefault()
	{
		// Tree
		char tree[MAX_STRUCTURE_HEIGHT][MAX_STRUCTURE_WIDTH] = {
			{ 0, 0, 0, 1, 1, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 0, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 0 },
			{ 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 0, 0, 0, 1, 1, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 0, 0, 0 } };
		addStructure("tree", tree);

		// TODO, add more structures to the world
	}
};
#undef STRUCTURES_BUILTIN