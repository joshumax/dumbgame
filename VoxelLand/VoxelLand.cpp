// VoxelLand.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "World.h"
#include <iostream>
#include <Windows.h>

int _tmain(int argc, _TCHAR* argv[])
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD coord{
		80,
		25
	};
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	World world(csbi.dwSize.Y, csbi.dwSize.X);
	world.generateWorld();
	//world.printData();
	world.printTerrainAtChunk(STD_OUTPUT_HANDLE, 0);
	std::cin.get();
	return 0;
}

